﻿using UnityEngine;
using System.Collections;

public class SimpleGhost : MonoBehaviour {

	public float speed;
	private Vector2 destination;
	private Vector2 direction;
	private Vector2 nextDirection;

	void Awake () {
		destination = Vector2.zero;
		direction = Vector2.zero;
		nextDirection = Vector2.zero;

	}
	// Use this for initialization
	void Start () {
		direction = Vector2.right;
		destination = transform.position;

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		Vector2 p = Vector2.MoveTowards (transform.position, direction, speed);
		GetComponent<Rigidbody2D> ().MovePosition (p);

		if (Vector2.Distance (destination, transform.position) < 0.00001f) {
			nextDirection = Vector2.zero;

			if (Valid (Vector2.right))
				nextDirection = Vector2.right;
			else if (Valid (Vector2.up))
				nextDirection = Vector2.up;
			else if (Valid (Vector2.down))
				nextDirection = Vector2.down;
			else if (Valid (Vector2.left))
				nextDirection = Vector2.left;
			else {
				if (Valid (direction))
					nextDirection = Vector2.zero;
			}
			destination = (Vector2)transform.position + nextDirection;
			direction = nextDirection;
		
		}
	}

	bool Valid (Vector2 diraction) {
		
		Vector2 pos = transform.position;
		diraction += new Vector2 (diraction.x * 0.45f, diraction.y * 0.45f);
		RaycastHit2D hit = Physics2D.Linecast (pos + diraction, pos);

		if (hit.collider == null) {
			return true;
		} else { 
			if (hit.transform == this.transform) {
				return true;
			} else if (hit.transform.CompareTag ("Item")) {
				return true;
			} else if (hit.transform.CompareTag ("Energizer")) {
				return true;
			} else if (hit.transform.CompareTag ("Player")) {
				return true;
			} else {
				return false;
			}
		}
	}
}
